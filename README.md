# Convert original project ( Andriod/JAVA to Xamarin - C#) #

Original project -> [https://github.com/lorensiuswlt/P25Demo](Link URL)

P25Demo
P25Demo is a sample to show how to print data from Android to Blue Bamboo P25 portable printer using Bluetooth. This demo shows how to print raw text, receipt, 1D dan 2D barcode and also bitmap image.

See [http://www.londatiga.net/it/programming/android/blue-bamboo-p25-printer-android-demo-application-with-source-code](Link URL) for more information.

Example Image

![prin.png](https://bitbucket.org/repo/pqqq4g/images/3150303174-prin.png)